export default class Component {
	tagName;
	children;
	attribute;

	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}

	render() {
		if (
			this.children == undefined ||
			(this.children == null && this.attribute != null)
		) {
			return `<${this.tagName} ${this.renderAttribute()} />`;
		}
		return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
	}
	renderAttribute() {
		return this.attribute == undefined || this.attribute == null
			? ``
			: `${this.attribute.name}="${this.attribute.value}"`;
	}
	renderChildren() {
		if (this.children instanceof String) {
			return this.children;
		}
		return this.children.join('');
	}
}
