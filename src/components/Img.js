import Component from './Component.js';
export default class Img extends Component {
	constructor(source) {
		super(
			'img',
			{
				name: 'src',
				value: source,
			},
			null
		);
	}
}
